# README #
This repository stores LaTeX source code for my Msc. thesis "Algorithmic Applications of Persistent Data Structures".
The accompanying code is hosted on [separate repository](https://bitbucket.org/sapalskimichal/persistent).
The paper in pdf format can be found on [Google Drive](https://drive.google.com/file/d/0B9yIvel1S5WzbjR2WDRjaG0yMFk).