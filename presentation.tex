% vim: spelllang=en
\documentclass[xcolor=dvipsnames]{beamer} %używamy nazw kolorów

\usepackage[polish,english]{babel}
\usepackage{fancybox}
\usepackage{graphicx}
\usepackage{default}
\usepackage{luatextra}
\defaultfontfeatures{Ligatures=TeX}

\usepackage{tikz}
% To use this libraries, you must have pgf 3.0
% (download http://sourceforge.net/projects/pgf/ and unzip to ~/texmf)
% and compile with lualatex
\usetikzlibrary{graphdrawing}
\usetikzlibrary{graphs}
\usegdlibrary{trees}
\usegdlibrary{layered}
\usetikzlibrary{calc}
\usetikzlibrary{quotes}
\usetikzlibrary{chains}
\usetikzlibrary{decorations.pathreplacing}
\usetikzlibrary{shapes.geometric}
\usetikzlibrary{positioning}

\usepackage{framed}

\usepackage{ifthen}

\usepackage{caption}
\usepackage{subcaption}

\usepackage{pgfpages}
%Notes:
%\setbeameroption{show notes}
%\setbeameroption{show notes on second screen=right}

\mode<presentation>
{
  \usetheme{default}      % or try Darmstadt, Madrid, Warsaw, ...
  \usecolortheme{default} % or try albatross, beaver, crane, ...
  \usefonttheme{default}  % or try serif, structurebold, ...
  \setbeamertemplate{navigation symbols}{}
  \setbeamertemplate{caption}[numbered]
} 
\title{Algorithmic Applications \\ of Persistent Data Structures}
\author{Michał Sapalski}
\date{2014-09-05}
\institute{Theoretical Computer Science @ Jagiellonian University}

\newcommand{\code}[1]{\texttt{#1}}
\newcommand{\codem}[1]{\mathtt{#1}}

\begin{document}
\definecolor{shaded}{gray}{0.9}
\section{}
\begin{frame}[plain]
\maketitle
\begin{center}
 \includegraphics[height=2cm]{tcs.pdf}
\end{center}
\end{frame}

\section{Introduction}
\begin{frame}
\frametitle{Presentation outline}
\tableofcontents
\end{frame}

\subsection{What is a Persistent Data Structure}
\begin{frame}
\frametitle{What is a Persistent Data Structure}
\begin{block}{}
A~data structure is \emph{persistent} if any update
operation does not modify the data structure, but returns a~new, updated,
version of~it.
\end{block}
\pause
\begin{center}
  \includegraphics<2>[width=0.8\textwidth]{persistent1.pdf}
  \includegraphics<3>[width=0.8\textwidth]{persistent2.pdf}
  \includegraphics<4>[width=0.8\textwidth]{persistent3.pdf}
\end{center}
\end{frame}

\subsection{Application in Computational Geometry}
\begin{frame}[t]
  \frametitle{Application in Computational Geometry}
\begin{block}{Point Location Problem}
  Given a~partition of the plane and a~set of points, determine to which region
  each of the points belongs.
\end{block}
\pause
\begin{center}
  \includegraphics<2>[width=0.8\textwidth]{point-location.pdf}
  \includegraphics<3>[width=0.8\textwidth]{point-location-sweep.pdf}
  \includegraphics<4>[width=0.8\textwidth]{point-location-sweeps.pdf}
\end{center}
\end{frame}

\subsection{The Framework}
\begin{frame}[t]
  \frametitle{The Framework}
\begin{block}{Range Sum Problem}
  Given an~array of numbers, preprocess it to quickly compute the sum of a~given
  slice of the array.
\end{block}

\def\samplearray{5,3,7,2,9,12,23,3,4,2}
\directlua{function rangesum(a,b)
    s = 0
    array = {\samplearray}
    for i=a,b do
      s = s + array[i+1]
    end
    return s
  end
}

\only<2>{
  \begin{figure}[t]
    \centering
    \scalebox{0.9}{
    \begin{tikzpicture}[
        start chain=1 going right,node distance=-0.15mm,
        every node/.style={minimum size=10mm}
      ]
      \foreach \x [count=\xi from 0] in \samplearray {
        \x, \node [draw,on chain=1] (input\xi) {\x};
        \draw (input\xi.north) node [yshift=4mm] {\xi};

      } 

      \draw [decorate,decoration={brace,amplitude=10pt,mirror,raise=1mm}]
      (input2.south west) -- (input7.south east) node [black,midway,yshift=-8mm]
      {$\code{RangeSum(2,7)}=\directlua{tex.print(rangesum(2,7))}$};
    \end{tikzpicture}
  }
  \end{figure}
}

\only<3>{
  \begin{figure}[t]
    \centering
    \scalebox{0.9}{
    \begin{tikzpicture}[
        start chain=1 going right,node distance=-0.15mm,
        every node/.style={minimum size=10mm}
      ]
      \foreach \x [count=\xi from 0] in \samplearray {
        \x, \node [draw,on chain=1] (input\xi) {\x};
        \draw (input\xi.north) node [yshift=4mm] {\xi};
      } 
      \draw (input2.south) node [yshift=-4mm,xshift=-5mm]
      {\directlua{tex.print(rangesum(0,1))}};
      \draw (input7.south) node [yshift=-4mm,xshift=+5mm]
      {\directlua{tex.print(rangesum(0,7))}};

      \draw [decorate,decoration={brace,amplitude=10pt,mirror,raise=8mm}]
      (input2.south west) -- (input7.south east) node [black,midway,yshift=-15mm]
      {$\code{RangeSum(2,7)}=\directlua{tex.print(rangesum(0,7))} -
      \directlua{tex.print(rangesum(0,1))}$};
    \end{tikzpicture}
  }
  \end{figure}
}

\only<4>{
  \begin{figure}[t]
    \centering
    \scalebox{0.9}{
    \begin{tikzpicture}[
        start chain=1 going right, start chain=2 going right,node distance=-0.15mm,
        every node/.style={minimum size=10mm}
      ]
      \foreach \x [count=\xi from 0] in \samplearray {
        \x, \node [draw,on chain=1] (input\xi) {\x};
        \draw (input\xi.north) node [yshift=4mm] {\xi};
      } 
      \node [draw,on chain=2,below of=input0,node distance=10mm] (prefixstart)
      [xshift=-5mm] {0};
      \foreach \x [count=\xi from 0] in \samplearray {
        \x, \node [draw,on chain=2] (prefix\xi)
        {\directlua{tex.print(rangesum(0,\xi))}};
      } 
    \end{tikzpicture}
  }
  \end{figure}
}
\end{frame}

\section{More Sophisticated Applications}
\begin{frame}
  \begin{center}
    \Huge{More Sophisticated Applications}
  \end{center}
\end{frame}

\subsection{Range Set Problem}
\begin{frame}[t]
\frametitle{Range Set Problem}
\only<1-3>{
\begin{block}{}
  Given an~array, preprocess it to quickly list all distinct elements in a~given
  slice of the array.
\end{block}
}
\only<2>{
\begin{figure}[H]
  \def\samplearray{c/0,b/1,d/0,a/1,c/0,d/1,c/1,b/0,e/0}
  \centering
  \scalebox{0.9}{
  \begin{tikzpicture}[
        start chain=1 going right,node distance=-0.15mm,
        every node/.style={minimum size=10mm}
      ]
      \foreach \x/\u [count=\xi from 0] in \samplearray {
        \x, \node [draw,on chain=1,text height=1.5ex, text depth=.25ex] (input\xi)
        {\x};
          \draw (input\xi.north) node [yshift=4mm] {\xi};
      } 
      \draw [decorate,decoration={brace,amplitude=10pt,mirror,raise=1mm}]
      (input2.south west) -- (input6.south east) node [black,midway,yshift=-8mm]
      {$\code{RangeSet(2,6)}=\lbrace$d, a, c$\rbrace$};
  \end{tikzpicture}
}
\end{figure}
}
\only<3-4>{
\begin{figure}[H]
  \def\samplearray{c/0,b/1,d/0,a/1,c/0,d/1,c/1,b/0,e/0}
  \centering
  \scalebox{0.9}{
  \begin{tikzpicture}[
        start chain=1 going right,node distance=-0.15mm,
        every node/.style={minimum size=10mm}
      ]
      \foreach \x/\u [count=\xi from 0] in \samplearray {
        \x, \node [draw,on chain=1,text height=1.5ex, text depth=.25ex] (input\xi)
        {\ifthenelse{\u=1}{\underline{\x}}{\x}};
          \draw (input\xi.north) node [yshift=4mm] {\xi};
      } 
      \draw [decorate,decoration={brace,amplitude=10pt,mirror,raise=1mm}]
      (input2.south west) -- (input6.south east) node [black,midway,yshift=-8mm]
      {$\code{RangeSet(2,6)}=\lbrace$d, a, c$\rbrace$};
  \end{tikzpicture}
}
\end{figure}
}

\only<4>{
\begin{figure}[htb]
  \newcommand{\leaf}[2]{\code{########1}: \code{########2}}
  \centering
  \scalebox{0.7}{
  \begin{tikzpicture}[every node/.style={rectangle, draw, minimum size=1cm}]
    \graph [tree layout, grow=down, fresh nodes, level distance=1.5cm,
    sibling distance=1.5cm]
    {
        "" -- {
          "" -- {
            "" -- {
              "\leaf{e}{ }",
              "\leaf{b}{1}"
            },
            "\leaf{a}{3}"
          },
          "" -- {
            "\leaf{d}{5}",
            "\leaf{c}{6}"
          }
        }
      };
  \end{tikzpicture}
  }
\end{figure}
}
\end{frame}

\subsection{Other Problems}
\begin{frame}
  \frametitle{Other Problems}
  \begin{itemize}
    \item Element Count
    \item Interval Count
    \item Set Size
    \item k-th Element
    \item Minority/Majority
    \item Minimum Spanning Forest Query
      \pause
    \item Level Ancestor Query
    \item Descendant Search Query
  \end{itemize}
\end{frame}

\section{Interesting Details}
\begin{frame}
  \begin{center}
    \Huge{Interesting Details}
  \end{center}
\end{frame}

\subsection{Generalization to Trees}
\begin{frame}[t]
  \frametitle{Generalization to Trees} 

\def\shortsamplearrayA{2,4,7,6}
\directlua{function shortrangesum(a,b)
    s = 0
    array = {\shortsamplearrayA}
    for i=a,b do
      s = s + array[i+1]
    end
    return s
  end
}
\only<1-2>{
  \begin{figure}[t]
    \centering
    \scalebox{0.9}{
    \begin{tikzpicture}[
        start chain=1 going right, start chain=2 going right,node distance=-0.15mm,
        every node/.style={minimum size=10mm}
      ]
      \foreach \x [count=\xi from 0] in \shortsamplearrayA {
        \x, \node [draw,on chain=1] (input\xi) {\x};
        \draw (input\xi.north) node [yshift=4mm] {\xi};
      } 
      \node [draw,on chain=2,below of=input0,node distance=10mm] (prefixstart)
      [xshift=-5mm] {0};
      \foreach \x [count=\xi from 0] in \shortsamplearrayA {
        \x, \node [draw,on chain=2] (prefix\xi)
        {\directlua{tex.print(shortrangesum(0,\xi))}};
      } 
    \end{tikzpicture}
  }
  \end{figure}
}

\def\shortsamplearrayB{2,4,1}
\directlua{function shortrangesum(a,b)
    s = 0
    array = {\shortsamplearrayB}
    for i=a,b do
      s = s + array[i+1]
    end
    return s
  end
}
\only<3>{
  \begin{figure}[t]
    \centering
    \scalebox{0.9}{
    \begin{tikzpicture}[
        start chain=1 going right, start chain=2 going right,node distance=-0.15mm,
        every node/.style={minimum size=10mm}
      ]
      \foreach \x [count=\xi from 0] in \shortsamplearrayB {
        \x, \node [draw,on chain=1] (input\xi) {\x};
        \draw (input\xi.north) node [yshift=4mm] {\xi};
      } 
      \node [draw,on chain=2,below of=input0,node distance=10mm] (prefixstart)
      [xshift=-5mm] {0};
      \foreach \x [count=\xi from 0] in \shortsamplearrayB {
        \x, \node [draw,on chain=2] (prefix\xi)
        {\directlua{tex.print(shortrangesum(0,\xi))}};
      } 
    \end{tikzpicture}
  }
  \end{figure}
}

\def\shortsamplearrayC{5,7,3}
\directlua{function shortrangesum(a,b)
    s = 0
    array = {\shortsamplearrayC}
    for i=a,b do
      s = s + array[i+1]
    end
    return s
  end
}
\only<4>{
  \begin{figure}[t]
    \centering
    \scalebox{0.9}{
    \begin{tikzpicture}[
        start chain=1 going right, start chain=2 going right,node distance=-0.15mm,
        every node/.style={minimum size=10mm}
      ]
      \foreach \x [count=\xi from 0] in \shortsamplearrayC {
        \x, \node [draw,on chain=1] (input\xi) {\x};
        \draw (input\xi.north) node [yshift=4mm] {\xi};
      } 
      \node [draw,on chain=2,below of=input0,node distance=10mm] (prefixstart)
      [xshift=-5mm] {0};
      \foreach \x [count=\xi from 0] in \shortsamplearrayC {
        \x, \node [draw,on chain=2] (prefix\xi)
        {\directlua{tex.print(shortrangesum(0,\xi))}};
      } 
    \end{tikzpicture}
  }
  \end{figure}
}
\begin{figure}[htb]
  \tikzset{
    every node/.style={circle, draw, minimum size=0.75cm, inner sep=0.5mm,text height=1.5ex, text depth=.25ex},
    level distance=1.6cm,
    level 1/.style={sibling distance=2.25cm},
    level 2/.style={sibling distance=1.5cm},
    label/.style={
      pos=0.5, draw=none, fill=white,
      inner sep=0mm, minimum size=4mm
    }
  }
  \centering
  \scalebox{0.7}{
  \begin{subfigure}[t]{0.3\textwidth}
\only<2-4>{
    \centering
    \begin{tikzpicture}
      \node {0}
        child { node {2}
          child { node {6}
            child { node {13}
              child {node {19}
                edge from parent[->] node[label] {6}
              }
              edge from parent[->] node[label] {7}
            }
            edge from parent[->] node[label] {4}
          }
          edge from parent[->] node[label] {2}
        };
    \end{tikzpicture}
  }
  \end{subfigure}
  \hspace{0.03\textwidth}
  \begin{subfigure}[t]{0.3\textwidth}
\only<3-4>{
    \centering
    \begin{tikzpicture}
      \node {0}
        child { node {2}
          child { node {6}
            child { node {13}
              child {node {19}
                edge from parent[->] node[label] {6}
              }
              edge from parent[->] node[label] {7}
            }
            child {node {7}
              edge from parent[->] node[label] {1}
            }
            edge from parent[->] node[label] {4}
          }
          edge from parent[->] node[label] {2}
        };
    \end{tikzpicture}
  }
  \end{subfigure}
  \hspace{0.03\textwidth}
  \begin{subfigure}[t]{0.3\textwidth}
    \only<4>{
    \centering
    \begin{tikzpicture}
      \node {0}
        child { node {2}
          child { node {6}
            child { node {13}
              child {node {19}
                edge from parent[->] node[label] {6}
              }
              edge from parent[->] node[label] {7}
            }
            child {node {7}
              edge from parent[->] node[label] {1}
            }
            edge from parent[->] node[label] {4}
          }
          edge from parent[->] node[label] {2}
        }
        child { node {5}
          child { node {12}
            child { node {15}
              edge from parent[->] node[label] {3}
            }
            edge from parent[->] node[label] {7}
          }
          edge from parent[->] node[label] {5}
        };
    \end{tikzpicture}
  }
  \end{subfigure}
}
\end{figure}
\begin{figure}[htb]
  \tikzset{
    every node/.style={circle, draw, minimum size=0.75cm, inner sep=0.5mm,text height=1.5ex, text depth=.25ex},
    level distance=1.6cm,
    level 1/.style={sibling distance=2.25cm},
    level 2/.style={sibling distance=1.5cm},
    label/.style={
      pos=0.5, draw=none, fill=white,
      inner sep=0mm, minimum size=4mm
    }
  }
  \only<5>{
  \begin{subfigure}[t]{0.5\textwidth}
    \centering
    \begin{tikzpicture}
      \node {0}
        child { node {5}
          child { node {12}
            child { node {15}
              edge from parent[->] node[label] {3}
            }
            edge from parent[->] node[label] {7}
          }
          edge from parent[->] node[label] {5}
        }
        child { node[very thick] {2}
          child { node {6}
            child { node {13}
              child {node[very thick] {19}
                edge from parent[->] node[label] {6}
              }
              edge from parent[->] node[label] {7}
            }
            child {node {7}
              edge from parent[->] node[label] {1}
            }
            edge from parent[->] node[label] {4}
          }
          edge from parent[->] node[label] {2}
        };
    \end{tikzpicture}
  \end{subfigure}
}
  \only<6>{
  \begin{subfigure}[t]{0.5\textwidth}
    \centering
    \begin{tikzpicture}
      \node {0}
        child { node {5}
          child { node {12}
            child { node {15}
              edge from parent[->] node[label] {3}
            }
            edge from parent[->] node[label] {7}
          }
          edge from parent[->] node[label] {5}
        }
        child { node {2}
          child { node[dashed, thick] {6}
            child { node {13}
              child {node[very thick] {19}
                edge from parent[->] node[label] {6}
              }
              edge from parent[->] node[label] {7}
            }
            child {node[very thick] {7}
              edge from parent[->] node[label] {1}
            }
            edge from parent[->] node[label] {4}
          }
          edge from parent[->] node[label] {2}
        };
    \end{tikzpicture}
  \end{subfigure}
}
\end{figure}
\end{frame}

\subsection{Tree Subtracting}
\begin{frame}
  \frametitle{Tree Subtracting}
  \only<1>{
\begin{figure}[htb]
  \def\samplearray{5,1,4,2,6,4,2,3,4,6}
  \def\ihigh{6}
  \centering
  \scalebox{0.9}{
  \begin{tikzpicture}[
        start chain=1 going right,
        node distance=-0.15mm,every node/.style={minimum size=10mm}
      ]

      \foreach \x [count=\xi from 0] in \samplearray {
        \ifthenelse{\directlua{if (\xi <= \ihigh) then tex.print(1) else tex.print(0) end}=1}{
          \x, \node [fill=shaded,draw,on chain=1] (x\xi) {\x};
        }{
          \x, \node [draw,on chain=1] (x\xi) {\x};
        }
      } 
  \end{tikzpicture}
}
\end{figure}
  }

  \only<2>{
\begin{figure}[htb]
  \def\samplearray{5,1,4,2,6,4,2,3,4,6}
  \def\ihigh{2}
  \centering
  \scalebox{0.9}{
  \begin{tikzpicture}[
        start chain=1 going right,
        node distance=-0.15mm,every node/.style={minimum size=10mm}
      ]

      \foreach \x [count=\xi from 0] in \samplearray {
        \ifthenelse{\directlua{if (\xi <= \ihigh) then tex.print(1) else tex.print(0) end}=1}{
          \x, \node [fill=shaded,draw,on chain=1] (x\xi) {\x};
        }{
          \x, \node [draw,on chain=1] (x\xi) {\x};
        }
      } 
  \end{tikzpicture}
}
\end{figure}
  }

  \only<3>{
\begin{figure}[htb]
  \def\samplearray{5,1,4,2,6,4,2,3,4,6}
  \def\ilow{3}
  \def\ihigh{6}
  \centering
  \scalebox{0.9}{
  \begin{tikzpicture}[
        start chain=1 going right,
        node distance=-0.15mm,every node/.style={minimum size=10mm}
      ]

      \foreach \x [count=\xi from 0] in \samplearray {
        \ifthenelse{\directlua{if (\ilow <= \xi) and (\xi <= \ihigh) then tex.print(1) else tex.print(0) end}=1}{
          \x, \node [fill=shaded,draw,on chain=1] (x\xi) {\x};
        }{
          \x, \node [draw,on chain=1] (x\xi) {\x};
        }
      } 
  \end{tikzpicture}
}
\end{figure}
  }
  
\end{frame}

\begin{frame}[t]
  \frametitle{Tree Subtracting}
\begin{figure}[htb]
  \newcommand{\branch}[2]{\code{####1}: \code{####2}}
  \newcommand{\leaf}[2]{\code{####1}: \code{####2}}
  \centering
  \scalebox{0.7}{
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
    \begin{tikzpicture}[every node/.style={rectangle, draw, minimum size=1cm},
      ]
      \graph [tree layout, grow=down, fresh nodes, level distance=1.5cm,
      sibling distance=1.5cm]
      {
        % a b c d
        "\branch{[a,d]}{3}" -- {
          "\branch{[a,b]}{1}" -- {
            "\leaf{a}{0}",
            "\leaf{b}{1}"
          },
          "\branch{[c,d]}{2}"[fill=shaded] -- {
            "\leaf{c}{1}",
            "\leaf{d}{1}"
          }
        }
      };
    \end{tikzpicture}
    \caption{Tree \code{x}.}
  \end{subfigure}
  \hspace{2cm}
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
    \begin{tikzpicture}[every node/.style={rectangle, draw, minimum size=1cm},
      ]
      \graph [tree layout, grow=down, fresh nodes, level distance=1.5cm,
      sibling distance=1.5cm]
      {
        % a b c d
        "\branch{[a,d]}{2}" -- {
          "\branch{[a,b]}{1}" -- {
            "\leaf{a}{0}",
            "\leaf{b}{1}"
          },
          "\branch{[c,d]}{1}"[fill=shaded] -- {
            "\leaf{c}{1}",
            "\leaf{d}{0}"
          }
        }
      };
    \end{tikzpicture}
    \caption{Tree \code{y}.}
  \end{subfigure}
}
\only<2>{
\scalebox{0.8}{
  \begin{subfigure}[t]{0.8\textwidth}
    \centering
    \begin{tikzpicture}[every node/.style={rectangle, draw, minimum size=1cm},
      ]
      \graph [tree layout, grow=down, fresh nodes, level distance=1.5cm,
      sibling distance=1.5cm]
      {
        % a b c d
        "\branch{[a,d]}{3-2=1}" -- {
          "\branch{[a,b]}{1-1=0}" -- {
            "\leaf{a}{0-0=0}",
            "\leaf{b}{1-1=0}"
          },
          "\branch{[c,d]}{2-1=1}"[fill=shaded] -- {
            "\leaf{c}{1-1=0}",
            "\leaf{d}{1-0=1}"
          }
        }
      };
    \end{tikzpicture}
    \caption{Tree \code{x - y}.}
  \end{subfigure}
}
}
\end{figure}

\end{frame}

\section{Additional Content}
\begin{frame}
  \frametitle{Additional Content}
  \begin{itemize}
    \item How to store a string in a persistent balanced binary tree.
    \item How to implement a persistent self-balancing binary tree.
    \item Implementation of solutions for all mentioned problems (with~tests).
  \end{itemize}
\end{frame}

\section*{Summary}
\begin{frame}
  \frametitle{Summary}
  \begin{itemize}
    \item Sweeping technique from computational geometry can be applied to
      non-geometric query problems thanks to persistent data structures.
    \item By leveraging full persistence of the sweep we often can generalize
      the solutions to work on trees.
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{center}
    \Huge{Questions}
  \end{center}
\end{frame}

\begin{frame}
  \begin{center}
    \Huge{Thank you for your attention}
  \end{center}
\end{frame}

\end{document}
