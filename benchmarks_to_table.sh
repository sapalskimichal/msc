#!/bin/sh

cat benchmark_results.txt | grep 'ns/op' | sed 's/Benchmark/        \\code{/' | \
  sed 's/\s\+[0-9]\+\s\+/} \& \\code{/' | sed 's/ ns\/op\s*$/}\\\\/'
