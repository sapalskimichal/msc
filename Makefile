msc.pdf: msc.tex point-location-sweep.pdf point-location-sweeps.pdf
	lualatex "$<"
	lualatex "$<"

presentation.pdf: presentation.tex tcs.pdf persistent1.pdf persistent2.pdf persistent3.pdf point-location.pdf point-location-sweep.pdf point-location-sweeps.pdf
	lualatex "$<"
	lualatex "$<"

figure.pdf: figure.tex
	lualatex "$<"
	lualatex "$<"

clean:
	mv tcs.pdf tcs-pdf
	rm -f *.aux *.log *.toc *.pdf
	mv tcs-pdf tcs.pdf

%.pdf: %.svg
	inkscape --without-gui --file="$<" --export-text-to-path --export-dpi=300 --export-area-page --export-pdf="$@"

all: msc.pdf figure.pdf presentation.pdf
